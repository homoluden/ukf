﻿namespace UKF.Filtering.Interfaces
{
    public interface IGpsPosition
    {
        double Latitude { get; }

        double Longitude { get; }

        double Altitude { get; }

        (double Longitude, double Latitude, double Altitude) ToPosTuple();
    }
}
