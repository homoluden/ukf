﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UKF.Filtering.Utils
{
    public class FixedSizeQueue<T>
    {
        private Queue<T> _q;
        private readonly object _lock = new object();

        public int Limit { get; set; }

        public FixedSizeQueue()
        {
            _q = new Queue<T>();
        }

        public FixedSizeQueue(int limit)
        {
            Limit = limit;
            _q = new Queue<T>(limit);
        }

        public T[] Items
        {
            get
            {
                lock (_lock)
                {
                    return _q.ToArray();
                }
            }
        }

        public int Count
        {
            get
            {
                lock (_lock)
                {
                    return _q.Count;
                }
            }
        }

        public void Enqueue(T obj)
        {
            if (_q.Count >= Limit)
            {
                lock (_lock)
                {
                    while (_q.Count > 0 && _q.Count >= Limit)
                    {
                        _q.Dequeue();
                    }
                }
            }

            _q.Enqueue(obj);
        }

        public void Clear()
        {
            lock (_lock)
            {
                _q = new Queue<T>(Limit);
            }
        }

        public async Task EnqueueManyAsync(T[] buffer)
        {
            var newQueue = await Task.Run(() =>
            {
                if (buffer.Length >= Limit)
                {
                    return new Queue<T>(buffer.Skip(buffer.Length - Limit).ToArray());
                }

                var skip = Limit - buffer.Length;

                T[] currentItems;
                lock (_lock)
                {
                    currentItems = _q.ToArray();
                }

                return new Queue<T>(currentItems.Skip(skip).Concat(buffer).ToArray());
            });

            lock (_lock)
            {
                _q = newQueue;
            }
        }

        public async Task<T[]> GetItemsAsync()
        {
            var items = await Task.Run(() => _q.ToArray());
            return items;
        }
    }
}
