﻿using System;
using System.Globalization;
using MathNet.Numerics.LinearAlgebra;

namespace UKF.Filtering
{
    public class Ssf
    {
        #region Fields

        private double _ts;
        private double _k;
        private Matrix<double> _a;
        private Matrix<double> _b;
        private Matrix<double> _c;
        private bool _tsSet;
        private bool _kSet;
        private bool _aSet;
        private bool _bSet;
        private bool _cSet;

        #endregion // Fields

        #region Properties

        public double TimeSample
        {
            get => _ts;
            set
            {
                _ts = value;
                _tsSet = value.CompareTo(double.Epsilon) >= 0;
            }
        }

        public double Gain
        {
            get => _k;
            set
            {
                _k = value;
                _kSet = !(double.IsInfinity(value) || double.IsNaN(value));
            }
        }

        public Matrix<double> A
        {
            get => _a;
            set
            {
                _a = value;
                _aSet = value != null;
            }
        }

        public Matrix<double> B
        {
            get => _b;
            set
            {
                _b = value;
                _bSet = value != null;
            }
        }

        public Matrix<double> C
        {
            get => _c;
            set
            {
                _c = value;
                _cSet = value != null;
            }
        }

        public double D { get; set; }

        public bool Initialized => _aSet && _bSet && _cSet && _tsSet && _kSet;

        public Matrix<double> State { get; private set; }

        #endregion // Properties

        #region Ctors

        public Ssf()
        {
        }

        //def initialize(a,b,c,d,ts,k)
        //    @a,@b,@c,@d,@ts,@t,@k = a,b,c,d,ts,0.0,k
        //    @x = NMatrix.float(@a.sizes[0],1) #Matrix.rows(Array.new(@a.row_size,[0.0]))
        //    @order = @a.sizes[1]#@a.row_size
        //end
        public Ssf(Matrix<double> f, Matrix<double> r, Matrix<double> m, double d, double ts, double k)
        {
            _ts = ts;
            _k = k;
            _c = m;
            _b = r;
            _a = f;
            D = d;

            State = Matrix<double>.Build.Dense(_a.RowCount, 1);
        }

        #endregion // Ctors

        #region Public Methods
        //def step(u) # public double Step(double U, double xnoise, double fnoise)
        //    tx 	= @a*@x + @b*u
        //    f = @c*@x
        //    @x = tx.clone
        //        @t += @ts
        //    return (f*@k)[0,0]
        //end
        public double Step(double u)
        {
            var x = (_a * State + _b * u);
            var y = (_c * State)[0, 0];
            State = x;
            return y * _k + D * u;
        }

        public double Step(double u, out double x0)
        {
            var res = Step(u);
            x0 = State[0, 0];
            return res;
        }


        //def Ssf.align_num_denum(num,denum)
        //        d = denum.length - num.length
        //    if  d >= 0 then
        //        num = num.reverse
        //        d.downto(1) {
        //            num << 0.0
        //        }
        //        num = num.reverse
        //    else
        //        d.upto(-1) {
        //            denum << 0.0
        //        }
        //    end
        //    return num.length,num,denum
        //end
        public static int AlignNumDenom(ref Vector<double> numerator, ref Vector<double> denominator)
        {
            if (numerator == null || denominator == null) return 0;

            int dl = denominator.Count - numerator.Count;
            if (dl >= 0)
            {
                var newNum = Vector<double>.Build.Dense(dl + numerator.Count);
                for (int i = 0; i < numerator.Count; i++)
                {
                    newNum[dl + i] = numerator[i];
                }
                //numerator = newNum.Add(numerator);
                numerator = newNum;
            }
            else
            {
                denominator = denominator.Add(Vector<double>.Build.Dense(-dl));
            }
            return numerator.Count;
        }

        //def Ssf.tf2ssd(num,denum,k,ts)# public static SSFilter TF2SSd(double[] Num, double[] Den, double k, double Ts)
        //    n,num,denum = Ssf::align_num_denum(num,denum)
        //    a = []
        //    b = []
        //    c = []
        //    (0).upto(n-3) {
        //        |i|
        //        ta = NArray.float(n-1)
        //        ta[i+1] = 1.0
        //        a << ta.to_a
        //        b << [0.0]
        //    }
        //    ta = []
        //    (n-1).downto(1) {
        //        |i|
        //        ta << -denum[i]/denum[0]
        //    }
        //    a << ta
        //    b << [1.0/denum[0]]
        //    d = num[0]/denum[0]

        //    c = NMatrix.float(n-1,1)
        //    (n-1).downto(1) {
        //        |i|
        //        c[i-1,0] = d*(-denum[i]) + num[i]
        //    }

        //    am = NMatrix.rows(a)    #Matrix.rows(a)
        //    bm = NMatrix.rows(b)    #Matrix.rows(b)
        //    e =  NMatrix.float(n-1,n-1).unit       #Matrix.xdiag(n-1,1.0)
        //    f = e + am*ts + am**2*ts**2/2 + am**3*ts**3/3
        //    r = (e*ts + am*ts**2/2 + am**2*ts**3/3)*bm #e*ts*bm
        //    ssf = Ssf.new(f,r,c,d,ts,k*denum.last/num.last)
        //    ssf
        //end
        public static Ssf C2DSS(Vector<double> num, Vector<double> denom, double gain, double timeSample)
        {
            int n = AlignNumDenom(ref num, ref denom);
            var a = Matrix<double>.Build.Dense(n - 1, n - 1);
            var b = Matrix<double>.Build.Dense(n - 1, 1, 0.0);
            for (int i = 0; i < n - 2; i++)
            {
                a[i, i + 1] = 1.0;
            }
            b[n - 2, 0] = 1 / denom[0];
            var d = num[0] / denom[0];
            for (int i = 0; i < n - 1; i++)
            {
                a[n - 2, i] = -denom[n - 1 - i] / denom[0];
            }
            var c = Matrix<double>.Build.Dense(1, n - 1);
            for (int i = 0; i < n - 1; i++)
            {
                c[0, i] = num[n - 1 - i] - denom[n - 1 - i] * d;
            }

            var e = Matrix<double>.Build.Dense(a.RowCount, a.RowCount);
            for (int i = 0; i < a.RowCount; i++)
            {
                e[i, i] = 1.0;
            }

            var f = e + a.Multiply(timeSample) +
                        a.Multiply(a).Multiply(timeSample * timeSample * 0.5);// +
                                                                              //a.Multiply(a).Multiply(a).Multiply(timeSample*timeSample*timeSample/3.0);
            var r = (e * timeSample + a.Multiply(timeSample * timeSample * 0.5)). // + a.Multiply(a).Multiply(Math.Pow(timeSample, 3)/3.0) ).
                        Multiply(b);
            return new Ssf(f, r, c, d, timeSample, gain * denom[n - 1] / num[n - 1]);
        }

        public static Ssf C2DSSStr(string numString, string denomString, double timeSample)
        {
            var numStrings = numString.Trim('[', ']').Split(" ,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var denomStrings = denomString.Trim('[', ']').Split(" ,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            var format = new NumberFormatInfo
            {
                NumberDecimalSeparator = "."
            };

            var num = new double[numStrings.Length];
            var denom = new double[denomStrings.Length];

            var i = 0;
            foreach (var str in numStrings)
            {
                num[i] = double.TryParse(str, NumberStyles.Float, format, out double val) ? val : 0.0;
                i++;
            }
            i = 0;
            foreach (var str in denomStrings)
            {
                denom[i] = double.TryParse(str, NumberStyles.Float, format, out double val) ? val : 0.0;
                i++;
            }
            return C2DSS(Vector<double>.Build.Dense(num), Vector<double>.Build.Dense(denom), 1.0, timeSample);
        }

        #endregion // Public Methods

    }
}