﻿using UKF.Filtering.Interfaces;

namespace UKF.Filtering.Data
{
    public class GpsRecord: IGpsPosition
    {
        public GpsRecord(
            double time,
            double longitude,
            double latitude,
            double altitude,
            float distX,
            float distY,
            float distZ,
            float positionAccuracy,
            float speed,
            double dt)
        {
            Time = time;
            Longitude = longitude;
            Latitude = latitude;
            Altitude = altitude;
            DistX = distX;
            DistY = distY;
            DistZ = distZ;
            PositionAccuracy = positionAccuracy;
            Speed = speed;
            Dt = dt;
        }

        public double Time { get; }

        public double Latitude { get; }

        public double Longitude { get; }

        public double Altitude { get; }

        public float DistX { get; }

        public float DistY { get; }

        public float DistZ { get; }

        public float PositionAccuracy { get; }

        public float Speed { get; }

        public double Dt { get; }

        public (double Longitude, double Latitude, double Altitude) ToPosTuple()
        {
            return (Longitude, Latitude, Altitude);
        }
    }

    public class AvgGpsPosition: IGpsPosition
    {
        public AvgGpsPosition(
            double longitude,
            double latitude,
            double altitude,
            double startTime,
            double endTime,
            double dt)
        {
            Longitude = longitude;
            Latitude = latitude;
            Altitude = altitude;
            StartTime = startTime;
            EndTime = endTime;
            Dt = dt;
        }

        public double Latitude { get; }

        public double Longitude { get; }

        public double Altitude { get; }

        public double StartTime { get; }

        public double EndTime { get; }

        public double Dt { get; }

        public (double Longitude, double Latitude, double Altitude) ToPosTuple()
        {
            return (Longitude, Latitude, Altitude);
        }
    }
}
