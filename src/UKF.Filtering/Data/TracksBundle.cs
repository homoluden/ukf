﻿namespace UKF.Filtering.Data
{
    public class TracksBundle
    {
        public AccelRecord[] AccelTrack { get; }
        public RotationRecord[] RotationTrack { get; }
        public GpsRecord[] GpsTrack { get; }

        public TracksBundle(AccelRecord[] accelTrack, RotationRecord[] rotationTrack, GpsRecord[] gpsTrack)
        {
            AccelTrack = accelTrack;
            RotationTrack = rotationTrack;
            GpsTrack = gpsTrack;
        }
    }
}
