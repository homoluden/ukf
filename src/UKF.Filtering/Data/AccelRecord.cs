﻿namespace UKF.Filtering.Data
{
    public class AccelRecord
    {
        public AccelRecord(
            double time = 0,
            float ax = 0,
            float ay = 0,
            float az = 0,
            float aex = 0,
            float aey = 0,
            float aez = 0,
            double dt = 0)
        {
            Time = time;
            Ax = ax;
            Ay = ay;
            Az = az;
            Aex = aex;
            Aey = aey;
            Aez = aez;
            Dt = dt;
        }

        public double Time { get; }

        public float Ax { get; }

        public float Ay { get; }

        public float Az { get; }

        public float Aex { get; }

        public float Aey { get; }

        public float Aez { get; }

        public double Dt { get; }
    }
}