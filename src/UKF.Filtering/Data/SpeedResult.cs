﻿namespace UKF.Filtering.Data
{
    public struct SpeedResult
    {
        public double AccSpeed;

        public double RawSpeed;

        public double AverageSpeed;

        public long Time;

        public float Ax;

        public float Ay;

        public float Az;

        public float Adt;
    }
}
