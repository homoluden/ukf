﻿namespace UKF.Filtering.Data
{
    public class RotationRecord
    {
        public RotationRecord(double time, float rx, float ry, float rz, float w, double dt)
        {
            Time = time;
            Rx = rx;
            Ry = ry;
            Rz = rz;
            W = w;
            Dt = dt;
        }

        public double Time { get; }
        public float Rx { get; }
        public float Ry { get; }
        public float Rz { get; }
        public float W { get; }
        public double Dt { get; }
    }
}
