﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;

namespace UKF.Filtering
{
    public class Ukf
    {
        private bool _initialized;
        private int[] _parameters;
        private ParamScale[] _paramScales;
        private int _n;
        private int _m;
        private double _ts;
        private Func<Matrix<double>, Matrix<double>> _h;
        private double _alpha;
        private double _beta;
        private double _ki;
        private double _r;
        private double _q;
        private double _a0;
        private double _a1;
        private double _a2;
        private double _a3;

        private Matrix<double> _Q;
        private Matrix<double> _R;
        private Vector<double> _X;
        private Matrix<double> _P;
        private double _lambda;
        private double _c;
        private Vector<double> _Wm;
        private Vector<double> _Wc;
        private Matrix<double> _F;

        public double[] State => _X.AsArray();

        public void Init(double ts,
                         int measurementLength,
                         Func<Matrix<double>, Matrix<double>> observation,
                         int[] parameters,
                         ParamScale[] paramScales)
        {
            _initialized = false;

            if (parameters.Length != paramScales.Length)
            {
                return;
            }

            _parameters = parameters;
            _paramScales = paramScales;

            _n = 9;
            _m = measurementLength;
            _ts = ts;
            _h = observation;

            ExtractAllParams();
            InitAdditionalParams();
            InitStateSpaceModel();

            _initialized = true;
        }

        public void Init(double ts,
                         int measurementLength,
                         Func<Matrix<double>, Matrix<double>> observation,
                         (double Alpha, double R, double Q) ukfParams,
                         (double A0, double A1, double A2, double A3) ssParams)
        {
            _initialized = false;

            _n = 9;
            _m = measurementLength;
            _ts = ts;
            _h = observation;

            _alpha = ukfParams.Alpha;
            _beta = 2;
            _ki = 0;
            _r = ukfParams.R;
            _q = ukfParams.Q;

            _a0 = ssParams.A0;
            _a1 = ssParams.A1;
            _a2 = ssParams.A2;
            _a3 = ssParams.A3;

            InitAdditionalParams();
            InitStateSpaceModel();

            _initialized = true;
        }

        public void Update(double[] measurements)
        {
            if (!_initialized)
            {
                return;
            }
            var sigmaPoints = GetSigmaPoints();
            sigmaPoints = TransformSigmaPoints(sigmaPoints);
            var statePropMean = CalcPredictedStateMean(sigmaPoints);
            var statePropCov = CalcPredictedStateCov(sigmaPoints, statePropMean);
            var sigmaObsProp = PropSigmaThroughObservation(sigmaPoints);
            var obsMean = CalcObservationMean(sigmaObsProp);
            var obsCov = CalcObservationCov(sigmaObsProp, obsMean);
            var crossCov = CalcCrossCov(sigmaPoints, statePropMean, sigmaObsProp, obsMean);

            var observation = Vector.Build.Dense(measurements);
            MeasurementUpdate(statePropMean, observation, obsMean, statePropCov, obsCov, crossCov);
        }

        private Matrix<double> GetSigmaPoints()
        {
            var sqrtP = _P.Cholesky().Factor;
            var sqrtCP = _c * sqrtP;
            var minusSqrtCP = -1 * sqrtCP;

            for (int i = 0; i < sqrtCP.ColumnCount; i++)
            {
                sqrtCP.SetColumn(i, _X + sqrtCP.Column(i));
                minusSqrtCP.SetColumn(i, _X + minusSqrtCP.Column(i));
            }

            var khi = Matrix.Build.Dense(_n, 2 * _n + 1);
            khi.SetColumn(0, _X);
            khi.SetSubMatrix(0, 1, sqrtCP);
            khi.SetSubMatrix(0, _n + 1, minusSqrtCP);

            return khi;
        }

        private Matrix<double> TransformSigmaPoints(Matrix<double> sigmaPoints)
        {
            var sigmaProp = Matrix.Build.Dense(_n, sigmaPoints.ColumnCount);

            for (int i = 0; i < sigmaPoints.ColumnCount; i++)
            {
                var sigmaPt = sigmaPoints.Column(i);
                var sigmaPtProp = _F * sigmaPt;
                sigmaProp.SetColumn(i, sigmaPtProp);
            }

            return sigmaProp;
        }

        private Vector<double> CalcPredictedStateMean(Matrix<double> sigmaPoints)
        {
            var statePropMean = Vector.Build.Dense(_n, 0.0);
            for (int i = 0; i < sigmaPoints.ColumnCount; i++)
            {
                statePropMean += _Wm[i] * sigmaPoints.Column(i);
            }

            return statePropMean;
        }

        private Matrix<double> CalcPredictedStateCov(Matrix<double> sigmaPoints, Vector<double> statePropMean)
        {
            var statePropCov = Matrix.Build.Dense(_n, _n, 0.0);
            for (int i = 0; i < sigmaPoints.ColumnCount; i++)
            {
                var sigmaStateDiff = sigmaPoints.Column(i) - statePropMean;
                statePropCov += _Wc[i] * sigmaStateDiff.OuterProduct(sigmaStateDiff);
            }

            return _Q + statePropCov;
        }

        private Matrix<double> PropSigmaThroughObservation(Matrix<double> sigmaPoints)
        {
            var sigmaObs = Matrix.Build.Dense(_m, sigmaPoints.ColumnCount, 0.0);
            for (int i = 0; i < sigmaPoints.ColumnCount; i++)
            {
                var sigmaPoint = Matrix.Build.Dense(_n, 1);
                sigmaPoint.SetColumn(0, sigmaPoints.Column(i));
                var obs = _h(sigmaPoint);
                sigmaObs.SetSubMatrix(0, i, obs);
            }

            return sigmaObs;
        }

        private Vector<double> CalcObservationMean(Matrix<double> observations)
        {
            var obsMean = Vector.Build.Dense(_m, 0.0);
            for (int i = 0; i < observations.ColumnCount; i++)
            {
                obsMean += _Wm[i] * observations.Column(i);
            }

            return obsMean;
        }

        private Matrix<double> CalcObservationCov(Matrix<double> observations, Vector<double> obsMean)
        {
            var obsPropCov = Matrix.Build.Dense(_m, _m, 0.0);
            for (int i = 0; i < observations.ColumnCount; i++)
            {
                var obsDiff = observations.Column(i) - obsMean;
                obsPropCov += _Wc[i] * obsDiff.OuterProduct(obsDiff);
            }

            return _R + obsPropCov;
        }

        private Matrix<double> CalcCrossCov(
            Matrix<double> stateProp,
            Vector<double> stateMean, 
            Matrix<double> obsProp, 
            Vector<double> obsMean)
        {
            var crossCov = Matrix.Build.Dense(_n, _m, 0.0);
            for (int i = 0; i < stateProp.ColumnCount; i++)
            {
                var stateDiff = stateProp.Column(i) - stateMean;
                var obsDiff = obsProp.Column(i) - obsMean;
                crossCov += _Wc[i] * stateDiff.OuterProduct(obsDiff);
            }

            return crossCov;
        }

        private void MeasurementUpdate(
            Vector<double> statePropMean,
            Vector<double> measurement,
            Vector<double> measMean,
            Matrix<double> statePropCov,
            Matrix<double> obsCov,
            Matrix<double> crossCov)
        {
            var K = crossCov * (obsCov.Inverse());
            _X = statePropMean + K * (measurement - measMean);
            _P = statePropCov - K * obsCov * K.Transpose();
        }

        private void InitAdditionalParams()
        {
            _Q = Matrix.Build.DenseDiagonal(_n, _q);
            _R = Matrix.Build.DenseDiagonal(_m, _r);
            _X = _q * Vector.Build.Random(_n);
            _P = Matrix.Build.DenseIdentity(_n);

            _lambda = _alpha * _alpha * (_n + _ki) - _n;
            var c = _n + _lambda;
            _c = Math.Sqrt(c);

            //weights for means
            _Wm = Vector.Build.Dense((2 * _n + 1), 0.5 / c);
            _Wm[0] = _lambda / c;

            //weights for covariance
            _Wc = Vector.Build.Dense((2 * _n + 1));
            _Wm.CopyTo(_Wc);
            _Wc[0] = _Wm[0] + 1 - _alpha * _alpha + _beta;
        }

        private void InitStateSpaceModel()
        {
            var a = BuildTxMatrix(( _a0, _a1, _a2, _a3 ));
            _F = Matrix.Build.Dense(_n, _n, 0.0);
            _F.SetSubMatrix(0, 0, a);
            _F.SetSubMatrix(3, 3, a);
            _F.SetSubMatrix(6, 6, a);

            var e = Matrix<double>.Build.DenseIdentity(_n, _n);

            _F = e + _F.Multiply(_ts) + _F.Multiply(_F).Multiply(_ts * _ts * 0.5);
        }

        private Matrix<double> BuildTxMatrix((double a0, double a1, double a2, double a3) coeffs)
        {
            var tx = Matrix.Build.Dense(3, 3, 0.0);
            tx[0, 1] = tx[1, 2] = 1.0;

            var a0 = coeffs.a0;
            var a1 = coeffs.a1;
            var a2 = coeffs.a2;
            var a3 = coeffs.a3;
            tx[2, 0] = -a3 / a0;
            tx[2, 1] = -a2 / a0;
            tx[2, 2] = -a1 / a0;

            return tx;
        }

        private void ExtractAllParams()
        {
            _alpha = ExtractParam(0);
            _beta = 2;
            _ki = 0;
            _r = ExtractParam(1);
            _q = ExtractParam(2);
            
            _a0 = ExtractParam(3);
            _a1 = ExtractParam(4);
            _a2 = ExtractParam(5);
            _a3 = ExtractParam(6);
        }

        private double ExtractParam(int idx)
        {
            var param = _parameters[idx];
            var scale = _paramScales[idx];
            return scale.Bias + param * scale.Factor;
        }

        public struct ParamScale
        {
            public double Bias;

            public double Factor;
        }
    }
}