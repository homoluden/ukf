﻿
using Android.App;
using Android.Content.PM;
using Android.Hardware;
using Android.Locations;
using Android.OS;
using Android.Views;
using UKF.Droid.Inertial;
using UKF.UI.Helpers;
using UKF.UI.ViewModels;
using Xamarin.Forms;

namespace UKF.Droid
{
    [Activity(Label = "UKF", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new UI.App());

            // To prevent goind into sleep
            this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);

            UiLog.Trace("Getting Main VM from IoC...");
            var mvm = DependencyService.Get<MainViewModel>();

            if (mvm != null)
            {
                UiLog.Trace("Main VM OK");

                mvm.SetRecorder(new InertialRecorder((LocationManager)GetSystemService(LocationService), (SensorManager)GetSystemService(SensorService)));
            }
        }
    }
}