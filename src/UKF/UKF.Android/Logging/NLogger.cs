﻿using NLog;
using System;
using System.Threading.Tasks;
using UKF.Droid.Logging;
using Xamarin.Forms;
using ILogger = UKF.UI.Interfaces.Logging.ILogger;

[assembly: Dependency(typeof(NLogLogger))]
namespace UKF.Droid.Logging
{
    public class NLogLogger : ILogger
    {
        private readonly Logger _log;

        public void Log(LogEventInfo logEvent)
        {
            _log.Log(logEvent);
        }
        
        public void RecordTaskException(WeakReference<object> s, WeakReference<UnobservedTaskExceptionEventArgs> e)
        {
            if (!e.TryGetTarget(out var args)) return;

            if (!s.TryGetTarget(out var sender)) return;

            if (!(args.Exception is Exception exception)) return;

            var errorMessage = $"Time: {DateTime.Now}; Source: {sender}\r\nError: Unhandled Task Exception\r\n{(string.IsNullOrEmpty(exception.StackTrace) ? exception.ToString() : exception.StackTrace)}\r\n";

            Error(errorMessage, sender, args);
        }

        public void RecordUnhandledException(WeakReference<object> s, WeakReference<UnhandledExceptionEventArgs> e)
        {
            if (!e.TryGetTarget(out var args)) return;

            if (!s.TryGetTarget(out var sender)) return;

            if (!(args.ExceptionObject is Exception exception)) return;

            var errorMessage = $"Time: {DateTime.Now}; Source: {sender}\r\nError: Unhandled Exception\r\n{(string.IsNullOrEmpty(exception.StackTrace) ? exception.ToString() : exception.StackTrace)}\r\n";

            Error(errorMessage, sender, args);
        }

        public NLogLogger(Logger log)
        {
            _log = log;
        }

        public void Fatal(string text, params object[] args)
        {
            _log.Fatal(text, args);
        }

        public void Error(string text, params object[] args)
        {
            _log.Error(text, args);
        }

        public void Warn(string text, params object[] args)
        {
            _log.Warn(text, args);
        }

        public void Debug(string text, params object[] args)
        {
            _log.Debug(text, args);
        }

        public void Trace(string text, params object[] args)
        {
            _log.Trace(text, args);
        }

        public void Info(string text, params object[] args)
        {
            _log.Info(text, args);
        }

    }
}