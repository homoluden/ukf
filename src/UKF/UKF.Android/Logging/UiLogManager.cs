﻿using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UKF.Droid.Logging;
using UKF.UI.Interfaces.Logging;
using Environment = Android.OS.Environment;
using ILogger = UKF.UI.Interfaces.Logging.ILogger;

[assembly: Xamarin.Forms.Dependency(typeof(UiLogManager))]
namespace UKF.Droid.Logging
{
    public class UiLogManager : ILogManager
    {
        private const string FileNamePattern = "[\\/\\\\]+([\\w\\.]+.[\\w]+)$";

        public UiLogManager()
        {
            LogManager.ThrowExceptions = false;
            LogManager.ThrowConfigExceptions = false;
            
            var config = new LoggingConfiguration();

            var logsDir = Path.Combine(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryDocuments).AbsolutePath, "Mothorn/RecLogs");
            if (!Directory.Exists(logsDir))
            {
                Directory.CreateDirectory(logsDir);
            }

            CreateLogRule(config, LogLevel.Fatal, logsDir);
            
            CreateLogRule(config, LogLevel.Error, logsDir);
            
            CreateLogRule(config, LogLevel.Warn, logsDir);
            
            CreateLogRule(config, LogLevel.Debug, logsDir);
            
            CreateLogRule(config, LogLevel.Trace, logsDir);
            
            CreateLogRule(config, LogLevel.Info, logsDir);
            
            LogManager.Configuration = config;
        }

        public ILogger GetLog([CallerFilePath] string callerFilePath = "")
        {
            var fileName = ExtractFileName(callerFilePath);

            var logger = LogManager.GetLogger(fileName);
            return new NLogLogger(logger);
        }

        private static void CreateLogRule(LoggingConfiguration config, LogLevel lvl, string folder)
        {
            var dt = $"{DateTime.Now:MM-dd-yy}";

            var lvlString = lvl.ToString().ToLower();

            var fileName = $"{dt}.{lvlString}.log";
            var filePath = Path.Combine(folder, fileName);
            
            var fileTarget = new FileTarget(lvlString)
            {
                Layout = new SimpleLayout(
                    "${processtime:invariant=true} | [ ${event-properties:item=tag} ] >> ${message}${newline}"
                    + (lvl == LogLevel.Error || lvl == LogLevel.Fatal 
                        ? "Exception:${newline}${exception:format=toString}${newline}Stack Trace:${newline}${stacktrace:format=Raw}${newline}${newline}"
                        : "")),
                
                FileName = filePath,
                FileNameKind = FilePathKind.Absolute,
                AutoFlush = true
            };
            
            config.AddTarget(lvlString, fileTarget);
            config.AddRuleForOneLevel(lvl, fileTarget);
        }

        private static string ExtractFileName(string fileName)
        {
            var match = Regex.Match(fileName, FileNamePattern);
            var name = match.Success ? match.Groups[1].Value : "";

            return name;
        }

    }
}