﻿using System.Linq;
using Android.Hardware;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Appccelerate.StateMachine;
using System.Threading.Tasks;
using Appccelerate.StateMachine.AsyncMachine.Events;
using UKF.Droid.Inertial;
using UKF.Filtering.Data;
using UKF.Filtering.Utils;
using UKF.UI.Enums;
using UKF.UI.Helpers;
using UKF.UI.Interfaces.Inertial;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(InertialRecorder))]
namespace UKF.Droid.Inertial
{
    public class InertialRecorder : Java.Lang.Object, ILocationListener, ISensorEventListener, ISensorRecorder
    {
#region Fields

        private const int TsGpsMs = 20;

#if DEBUG
        private const int StateCheckPeriodMs = 300;
        private const int FixTrackLimit = 20;
#else
        private const int StateCheckPeriodMs = 200;
        private const int FixTrackLimit = 100;
#endif

        private const int AccelTrackLimit = 4000;
        private const int GpsTrackLimit = 100;

        private readonly FixedSizeQueue<AccelRecord> _accelTrack = new FixedSizeQueue<AccelRecord>() { Limit = AccelTrackLimit * 2 };
        private readonly FixedSizeQueue<RotationRecord> _rotTrack = new FixedSizeQueue<RotationRecord>() { Limit = AccelTrackLimit * 2 };
        private readonly FixedSizeQueue<GpsRecord> _gpsTrack = new FixedSizeQueue<GpsRecord>() { Limit = GpsTrackLimit * 2 };

        private readonly AsyncPassiveStateMachine<StateMachine.EStates, StateMachine.EEvents> _fsm = new AsyncPassiveStateMachine<StateMachine.EStates, StateMachine.EEvents>();
        private readonly LocationManager _location;
        private readonly SensorManager _sensors;
        private readonly Sensor _accel;
        private readonly Sensor _rot;
        private AvgGpsPosition _initPos;
        private RotationRecord _lastRotRecord;
        private AccelRecord _lastAccelRecord;
        private GpsRecord _lastGpsRecord;
        private long _initAccDt;
        private long _initRotDt;

#endregion

#region Properties

        public StateMachine.EStates CurrentState { get; private set; }
        public double ProgressPercent { get; private set; }

#endregion // Properties

#region .ctors

        public InertialRecorder(LocationManager location, SensorManager sensors)
        {
            DefineStateMachine();

            _location = location;
            _sensors = sensors;
            _accel = _sensors.GetDefaultSensor(SensorType.LinearAcceleration);
            _rot = _sensors.GetDefaultSensor(SensorType.RotationVector);

            _fsm.TransitionCompleted += (sender, args) =>
            {
                if (args.StateId == args.NewStateId) return;

                CurrentState = args.NewStateId;

#if DEBUG
                UiLog.Trace($"State changed to '{CurrentState}'");
#endif
            };
        }

#endregion // .ctors

#region Public Methods

        public async void Start()
        {
            await _fsm.FirePriority(StateMachine.EEvents.Stop);
            Device.BeginInvokeOnMainThread(StartListening);
            await _fsm.Fire(StateMachine.EEvents.Next);
        }

        public async void Restart()
        {
            await _fsm.FirePriority(StateMachine.EEvents.Restart);
            Device.BeginInvokeOnMainThread(StartListening);
        }

        public async void Stop()
        {
            await _fsm.FirePriority(StateMachine.EEvents.Stop);
            Device.BeginInvokeOnMainThread(StopListening);
        }

        public TracksBundle GetTracks()
        {
            return new TracksBundle(_accelTrack.Items, _rotTrack.Items, _gpsTrack.Items);
        }

#endregion // Public Methods

#region Sensor Listening

        public void OnLocationChanged(Location location)
        {
            var (dx, dy, dz) = 
                _initPos != null 
                ? NavigationHelper.GetDistances(_initPos.ToPosTuple(), (location.Longitude, location.Latitude, location.Altitude))
                : (0, 0, 0);

            var time = location.ElapsedRealtimeNanos / 1000000000.0;

            var newGpsRec = new GpsRecord(
                time,
                location.Longitude,
                location.Latitude,
                location.Altitude,
                dx,
                dy,
                dz,
                location.Accuracy,
                location.Speed,
                (time - _lastGpsRecord?.Time) ?? 0);

            _gpsTrack.Enqueue(_lastGpsRecord = newGpsRec);

#if DEBUG
            UiLog.Trace($"Location Changed to [{location.Longitude} ; {location.Latitude}] (LONG ; LAT)");
#endif
        }

        public void OnSensorChanged(SensorEvent e)
        {
            var time = e.Timestamp / 1000000000.0;

            switch (e.Sensor.Type)
            {
                case SensorType.LinearAcceleration:
                    var rot = _lastRotRecord ?? new RotationRecord(0, 0, 0, 0, 0, 0);
                    (float W, float Rx, float Ry, float Rz) rv = (rot.W, rot.Rx, rot.Ry, rot.Rz);
                    (float Ax, float Ay, float Az) accLoc = (e.Values[0], e.Values[1], e.Values[2]);
                    var accGeo = NavigationHelper.ProjectAccelToEarth(accLoc, (rv.W, rv.Rx, rv.Ry, rv.Rz));

                    var newAccelRec = new AccelRecord(
                        time, 
                        accLoc.Ax,
                        accLoc.Ay,
                        accLoc.Az,
                        accGeo.Aex,
                        accGeo.Aey,
                        accGeo.Aez,
                        (time - _lastAccelRecord?.Time) ?? 0);

                    _accelTrack.Enqueue(_lastAccelRecord = newAccelRec);
                    break;
                case SensorType.RotationVector:
                    var newRotRec = new RotationRecord(
                        time,
                        e.Values[1], // X
                        e.Values[2], // Y
                        e.Values[3], // Z
                        e.Values[0], // Angle
                        (time - _lastRotRecord?.Time) ?? 0);

                    _rotTrack.Enqueue(_lastRotRecord = newRotRec);
                    break;
            }
        }

        public void OnProviderDisabled(string provider)
        {
            UiLog.Warn("OnProviderDisabled not implemented!");
        }

        public void OnProviderEnabled(string provider)
        {
            UiLog.Warn("OnProviderEnabled not implemented!");
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            UiLog.Warn("OnStatusChanged not implemented!");
        }

        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {
            UiLog.Warn("OnAccuracyChanged not implemented!");
        }

#endregion // Sensor Listening

#region Private Methods

#region FSM Actions

        private static async Task LogLastError(string err)
        {
            await Task.Run(() => { UiLog.Error($"Failed to test Ukf. Latest Error:\n\n{err}\n\n"); });
        }

        private void ClearTracks()
        {
#if DEBUG
            UiLog.Trace($"Clearing Tracks...");
#endif

            _accelTrack.Clear();
            _rotTrack.Clear();
            _gpsTrack.Clear();
        }

        // Take initial position (lat; long; alt)
        // AVG by 100 pts.
        private async Task CheckInitialFix()
        {
            await Task.Delay(StateCheckPeriodMs);

            if (_gpsTrack.Count < FixTrackLimit)
            {
                ProgressPercent = 1.0 * _gpsTrack.Count / FixTrackLimit;

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                _fsm.Fire(StateMachine.EEvents.Proceed);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            }
            else
            {
                var t1 = NavigationHelper.CalculateInitialFix(_gpsTrack.Items);
                var t2 = NavigationHelper.CalculateAccDt(_accelTrack.Items);
                var t3 = NavigationHelper.CalculateRotDt(_rotTrack.Items);

                ClearTracks();

                await Task.WhenAll(t1, t2, t3);

                _initPos = t1.Result;
                _initAccDt = t2.Result;
                _initRotDt = t3.Result;

                await _fsm.Fire(StateMachine.EEvents.Next);
            }
        }

        // Accumulate Inertial data (1000 pts):
        // Time Dt Ax Ay Az Long Lat Alt Xgps Ygps Zgps Roll Pitch Yaw
        private async Task CheckRecordedData()
        {
            await Task.Delay(StateCheckPeriodMs);

            if (_accelTrack.Count < AccelTrackLimit)
            {
                ProgressPercent = 1.0 * _accelTrack.Count / AccelTrackLimit;

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                _fsm.Fire(StateMachine.EEvents.Proceed);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            }
            else
            {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                _fsm.Fire(StateMachine.EEvents.Next);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            }
        }

        // Output all data into CSV
        private async Task ReportData()
        {
            var accItems = _accelTrack.Items;
            var rotItems = _rotTrack.Items;
            var gpsItems = _gpsTrack.Items;

            var timeShift = accItems[0].Time;

            var accShift = Task.Run(() =>
            {
                var records = accItems.Select((acc) => new AccelRecord(acc.Time - timeShift, acc.Ax, acc.Ay, acc.Az, acc.Aex, acc.Aey, acc.Aez, acc.Dt))
                    .ToArray();
                return (dynamic)(new { Records = records });
            });

            var rotShift = Task.Run(() =>
            {
                var records = rotItems.Select((rot) => new RotationRecord(rot.Time - timeShift, rot.Rx, rot.Ry, rot.Rz, rot.W, rot.Dt)).ToArray();
                return (dynamic)(new { Records = records });
            });

            var gpsShift = Task.Run(() =>
            {
                var records = gpsItems.Select((gps) => new GpsRecord(gps.Time - timeShift, gps.Longitude, gps.Latitude, gps.Altitude, gps.DistX, gps.DistY, gps.DistZ, gps.PositionAccuracy, gps.Speed, gps.Dt)).ToArray();
                return (dynamic)(new { Records = records });
            });

            var shiftedItems = await Task.WhenAll(accShift, rotShift, gpsShift).ConfigureAwait(false);

            await ReportHelper.WriteDataAsync("records", shiftedItems[0].Records, shiftedItems[1].Records, shiftedItems[2].Records);

            ProgressPercent = 0;
            ClearTracks();

            await _fsm.Fire(StateMachine.EEvents.Next);
        }

        private void DefineStateMachine()
        {
            _fsm.In(StateMachine.EStates.Idle)
                .ExecuteOnEntry(() => StopListening())
                .ExecuteOnEntry(() => ClearTracks())
                .On(StateMachine.EEvents.Next).Goto(StateMachine.EStates.TakingFix);

            _fsm.In(StateMachine.EStates.TakingFix)
                .ExecuteOnEntry(CheckInitialFix)
                .On(StateMachine.EEvents.Proceed).Execute(CheckInitialFix)
                .On(StateMachine.EEvents.Restart).Goto(StateMachine.EStates.TakingFix)
                .On(StateMachine.EEvents.Next).Goto(StateMachine.EStates.RecordingData)
                .On(StateMachine.EEvents.Stop).Goto(StateMachine.EStates.Idle)
                .On(StateMachine.EEvents.Error).Goto(StateMachine.EStates.Error);

            _fsm.In(StateMachine.EStates.RecordingData)
                .ExecuteOnEntry(CheckRecordedData)
                .On(StateMachine.EEvents.Proceed).Execute(CheckRecordedData)
                .On(StateMachine.EEvents.Restart).Goto(StateMachine.EStates.RecordingData)
                .On(StateMachine.EEvents.Next).Goto(StateMachine.EStates.Done)
                .On(StateMachine.EEvents.Stop).Goto(StateMachine.EStates.Idle)
                .On(StateMachine.EEvents.Error).Goto(StateMachine.EStates.Error);

            _fsm.In(StateMachine.EStates.Done)
                .ExecuteOnEntry(() => StopListening())
                .ExecuteOnEntry(ReportData)
                .ExecuteOnEntry(() => ClearTracks())
                .On(StateMachine.EEvents.Next).Goto(StateMachine.EStates.Idle)
                .On(StateMachine.EEvents.Stop).Goto(StateMachine.EStates.Idle)
                .On(StateMachine.EEvents.Error).Goto(StateMachine.EStates.Error);

            _fsm.In(StateMachine.EStates.Error).ExecuteOnEntry<string>(LogLastError);

            _fsm.Initialize(CurrentState = StateMachine.EStates.Idle);

            _fsm.TransitionCompleted += FSM_TransitionCompleted;
            _fsm.TransitionExceptionThrown += FSM_TransitionExceptionThrown;

            Task.Run(() => _fsm.Start());
        }

        private void FSM_TransitionExceptionThrown(object sender, TransitionExceptionEventArgs<StateMachine.EStates, StateMachine.EEvents> e)
        {
            var errorMsg = e.Exception.ToString();

            Task.Run(() => _fsm.FirePriority(StateMachine.EEvents.Error, errorMsg));
        }

        private void FSM_TransitionCompleted(object sender, TransitionCompletedEventArgs<StateMachine.EStates, StateMachine.EEvents> e)
        {
            if (CurrentState == e.NewStateId) return;

            CurrentState = e.NewStateId;
        }

        #endregion // FSM Actions

        private void StartListening()
        {
#if DEBUG
            UiLog.Trace($"Sensor poll Activated...");
#endif

            _location.RequestLocationUpdates(LocationManager.GpsProvider, TsGpsMs, 0.1f, this);

            if (_accel != null && _rot != null)
            {
                _sensors.RegisterListener(this, _accel, SensorDelay.Fastest);
                _sensors.RegisterListener(this, _rot, SensorDelay.Fastest);
            }
            else
            {
                UiLog.Error("AccelerationTracker >> Accelerometer sensor is not found on device!");
            }
        }

        private void StopListening()
        {
#if DEBUG
            UiLog.Trace($"Sensor poll Stopped.");
#endif

            _location.RemoveUpdates(this);
            _sensors.UnregisterListener(this, _accel);
            _sensors.UnregisterListener(this, _rot);
        }

        protected override void Dispose(bool disposing)
        {
            StopListening();

            _accel.Dispose();
            _rot.Dispose();
            _sensors.Dispose();
            _location.Dispose();

            base.Dispose(disposing);
        }

#endregion // Private Methods
    }
}