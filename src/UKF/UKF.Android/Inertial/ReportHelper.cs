﻿using CsvHelper;
using Java.Util.Zip;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using UKF.Filtering.Data;
using UKF.UI.Helpers;
using Environment = Android.OS.Environment;

namespace UKF.Droid.Inertial
{
    public sealed class ReportHelper
    {
        private static readonly string _outputDir = Path.Combine(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryDocuments).AbsolutePath, "Mothorn/Reports");

        public static async Task WriteDataAsync(
            string reportName,
            AccelRecord[] accelRecords, 
            RotationRecord[] rotationRecords, 
            GpsRecord[] gpsRecords)
        {
            var sb = new StringBuilder();
            string accString, rotString, gpsString;

            using (var sw = new StringWriter(sb))
            using (var csv = new CsvWriter(sw))
            {
                csv.Configuration.QuoteNoFields = true;
                csv.Configuration.Delimiter = ";";

                csv.WriteHeader<AccelRecord>();
                csv.NextRecord();
                csv.WriteRecords(accelRecords);
                accString = sb.ToString();
                sb.Clear();

                csv.WriteHeader<RotationRecord>();
                csv.NextRecord();
                csv.WriteRecords(rotationRecords);
                rotString = sb.ToString();
                sb.Clear();

                csv.WriteHeader<GpsRecord>();
                csv.NextRecord();
                csv.WriteRecords(gpsRecords);
                gpsString = sb.ToString();
                sb.Clear();
            }

            await WriteReportZipAsync(reportName, new[]
            {
                ("acc", accString),
                ("rot", rotString),
                ("gps", gpsString)
            });
        }

        private static async Task<bool> WriteReportZipAsync(string reportName, (string Name, string Payload)[] entries)
        {
            if (!GetIsReportDirOk())
                return false;

            using (var fs = File.OpenWrite(Path.Combine(_outputDir, $"{reportName}-{DateTime.Now:MM-dd-yy-(hh-mm)}.zip")))
            using (var zip = new ZipOutputStream(fs))
            {
                foreach (var entry in entries)
                {
                    using (var zipEntry = new ZipEntry($"{entry.Name}.csv"))
                    {
                        var data = await Task.Run(() => Encoding.UTF8.GetBytes(entry.Payload));
                        await zip.PutNextEntryAsync(zipEntry);
                        await zip.WriteAsync(data);
                        zip.CloseEntry();
                    }
                }

                zip.Close();
            }

            return true;
        }

        private static bool GetIsReportDirOk()
        {
            try
            {
                if (!Directory.Exists(_outputDir))
                {
                    Directory.CreateDirectory(_outputDir);
                }

                return true;
            }
            catch (Exception e)
            {
                UiLog.Error("Failed to create Stats folder!", exception: e);
            }

            return false;
        }
    }
}