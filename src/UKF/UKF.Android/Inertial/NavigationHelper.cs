﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Hardware;
using Android.Locations;
using UKF.Filtering.Data;
using UKF.Filtering.Interfaces;

namespace UKF.Droid.Inertial
{
    public sealed class NavigationHelper
    {

        public static (float Dx, float Dy, float Dz) GetDistances(IGpsPosition from, IGpsPosition to)
        {
            return GetDistances(from.ToPosTuple(), to.ToPosTuple());
        }

        public static (float Dx, float Dy, float Dz) GetDistances(
            (double Longitude, double Latitude, double Altitude) from,
            (double Longitude, double Latitude, double Altitude) to)
        {
            // X - Longitude
            // Y - Latitude
            // Z - Altitude

            var result = new float[1];

            // Longitude first => distX
            Location.DistanceBetween(from.Latitude, from.Longitude, from.Latitude, to.Longitude, result);

            var distX = result[0];

            // Latitude now => distY
            Location.DistanceBetween(from.Latitude, from.Longitude, to.Latitude, from.Longitude, result);

            var distY = result[0];

            var distZ = to.Altitude - from.Altitude;

            return (distX, distY, (float)distZ);
        }

        public static (float Aex, float Aey, float Aez) ProjectAccelToEarth(
            (float Ax, float Ay, float Az) acceleration,
            (float W, float Rx, float Ry, float Rz) rv)
        {
            var rm = new float[9];
            SensorManager.GetRotationMatrixFromVector(rm, new []{ rv.W, rv.Rx, rv.Ry, rv.Rz });

            float ax = acceleration.Ax, ay = acceleration.Ay, az = acceleration.Az;

            var aex = rm[0] * ax + rm[3] * ay + rm[6] * az;
            var aey = rm[1] * ax + rm[4] * ay + rm[7] * az;
            var aez = rm[2] * ax + rm[5] * ay + rm[8] * az;

            return (aex, aey, aez);
        }

        public static async Task<AvgGpsPosition> CalculateInitialFix(GpsRecord[] gpsRecords)
        {
            var initFix = await Task.Run(() =>
            {
                var avgPos = new AvgGpsPosition(
                    gpsRecords.Select(r => r.Longitude).Average(),
                    gpsRecords.Select(r => r.Latitude).Average(),
                    gpsRecords.Select(r => r.Altitude).Average(),
                    gpsRecords.First().Time,
                    gpsRecords.Last().Time,
                    (long)Math.Round(gpsRecords.Select(r => r.Dt).Average(), 0)
                    );

                return avgPos;
            });

            return initFix;
        }

        public static async Task<long> CalculateAccDt(AccelRecord[] accRecords)
        {
            var avgDt = await Task.Run(() =>
            {
                return (long)Math.Round(accRecords.Select(r => r.Dt).Average(), 0);
            });

            return avgDt;
        }

        public static async Task<long> CalculateRotDt(RotationRecord[] rotRecords)
        {
            var avgDt = await Task.Run(() =>
            {
                return (long)Math.Round(rotRecords.Select(r => r.Dt).Average(), 0);
            });

            return avgDt;
        }

    }
}