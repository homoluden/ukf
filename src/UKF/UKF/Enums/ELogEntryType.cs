﻿namespace UKF.UI.Enums
{
    public enum ELogEntryType
    {
        Debug,
        Warning,
        Error,
        Wtf,
        Info
    }
}
