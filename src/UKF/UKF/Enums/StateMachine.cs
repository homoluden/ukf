﻿namespace UKF.UI.Enums
{
    public sealed class StateMachine
    {
        public enum EStates
        {
            Idle = 0,
            TakingFix,
            RecordingData,
            Done,
            Error
        }

        public enum EEvents
        {
            Proceed,
            Next,
            Stop,
            Restart,
            Error
        }
    }
}