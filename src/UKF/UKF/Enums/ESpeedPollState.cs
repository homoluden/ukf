﻿namespace UKF.UI.Enums
{
    public enum ESpeedPollState
    {
        Active,
        Stopped
    }
}
