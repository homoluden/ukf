﻿using UKF.UI.ViewModels;
using Xamarin.Forms;

namespace UKF.UI
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();


            DependencyService.Register<MainViewModel>();

            var mvm = DependencyService.Get<MainViewModel>();

            BindingContext = mvm;

        }
    }
}
