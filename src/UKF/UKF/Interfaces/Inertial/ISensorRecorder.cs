﻿using UKF.Filtering.Data;
using UKF.UI.Enums;

namespace UKF.UI.Interfaces.Inertial
{
    public interface ISensorRecorder
    {
        StateMachine.EStates CurrentState { get; }
        double ProgressPercent { get; }

        void Start();
        void Restart();
        void Stop();
        TracksBundle GetTracks();
    }
}
