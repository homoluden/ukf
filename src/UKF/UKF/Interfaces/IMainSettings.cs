﻿namespace UKF.UI.Interfaces
{
    public interface IMainSettings
    {
        string Version { get; set; }
    }
}
