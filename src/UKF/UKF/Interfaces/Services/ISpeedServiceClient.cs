﻿namespace UKF.UI.Interfaces.Services
{
    public interface ISpeedServiceClient
    {
        void Stop();

        void Start();
    }
}
