﻿using System.Threading.Tasks;
using UKF.UI.ViewModels;

namespace UKF.UI.Interfaces.Services
{
    public interface ISettingsService
    {
        Task SaveAsync(IMainSettings settings);

        Task<bool> LoadAsync(MainViewModel vm);
    }
}
