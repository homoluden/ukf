﻿using System;
using System.Threading.Tasks;
using NLog;

namespace UKF.UI.Interfaces.Logging
{
    public interface ILogger
    {
        void Trace(string text, params object[] args);
        void Debug(string text, params object[] args);
        void Info(string text, params object[] args);
        void Warn(string text, params object[] args);
        void Error(string text, params object[] args);
        void Fatal(string text, params object[] args);
        void Log(LogEventInfo logEvent);
        
        void RecordTaskException(WeakReference<object> s, WeakReference<UnobservedTaskExceptionEventArgs> e);
        void RecordUnhandledException(WeakReference<object> s, WeakReference<UnhandledExceptionEventArgs> e);
    }
}
