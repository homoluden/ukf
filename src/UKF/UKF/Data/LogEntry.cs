﻿using System;
using UKF.UI.Enums;

namespace UKF.UI.Data.Messages
{
    public class LogEntry
    {
        public DateTime Time { get; set; }
        public string Tag { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public ELogEntryType EntryType { get; set; }
    }
}
