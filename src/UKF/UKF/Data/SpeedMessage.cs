﻿using UKF.UI.Enums;

namespace UKF.UI.Data
{
    public class SpeedMessage
    {
        public double AccSpeed { get; set; }
        public double GpsSpeed { get; set; }
        public double AverageSpeed { get; set; }
        public string Time { get; set; }
        public ESpeedPollState PollState { get; set; }
        public double Ax { get; set; }
        public double Ay { get; set; }
        public double Az { get; set; }
        public double Adt { get; set; }
    }
}
