﻿using System.Threading.Tasks;
using System.Windows.Input;
using UKF.UI.Enums;
using UKF.UI.Interfaces.Inertial;
using Xamarin.Forms;

namespace UKF.UI.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private ISensorRecorder _recorder;

        #endregion //Fields

        #region Properties

        public ICommand StopCommand { get; }

        public ICommand StartCommand { get; }

        public StateMachine.EStates RecordingState
        {
            get => GetValue<StateMachine.EStates>();
            set
            {
                RecordingStateName = value.ToString();
                SetValue(value);
            }
        }

        public string RecordingStateName
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public double ProgressPercent
        {
            get => GetValue<double>();
            set => SetValue(value);
        }

        #endregion // Properties

        #region .ctors

        public MainViewModel()
        {
            StopCommand = new Command(() =>
            {
                _recorder.Stop();
                ProgressPercent = 0;
            });

            StartCommand = new Command(async () =>
            {
                if (_recorder == null) return;

                _recorder.Start();
                while (_recorder.CurrentState != StateMachine.EStates.Idle || _recorder.CurrentState != StateMachine.EStates.Error)
                {
                    RecordingState = _recorder.CurrentState;
                    ProgressPercent = _recorder.ProgressPercent;
                    await Task.Delay(100);
                }

                RecordingState = _recorder.CurrentState;
                ProgressPercent = 0;
            });
        }

        #endregion // .ctors

        #region Public Methods

        public void SetRecorder(ISensorRecorder sensorRecorder)
        {
            _recorder = sensorRecorder;
            RecordingState = _recorder.CurrentState;
        }

        #endregion // Public Methods

        #region Private Methods

        #endregion // Private Methods
    }
}