﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using NLog;
using UKF.UI.Data.Messages;
using UKF.UI.Enums;
using UKF.UI.Interfaces.Logging;
using Xamarin.Forms;
using ILogger = UKF.UI.Interfaces.Logging.ILogger;

namespace UKF.UI.Helpers
{
    public static class UiLog
    {
        private static ILogger Log => DependencyService.Get<ILogManager>().GetLog();
        
        private const string FileNamePattern = "[\\/\\\\]+([\\w\\.]+.[\\w]+)$";

        public static void Fatal(string message, string tag = null, [CallerFilePath] string fileName = "", object exception = null)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                tag = GenerateTag(fileName);
            }

            var logEvent = new LogEventInfo(LogLevel.Fatal, "", message);

            var exc = exception as Exception ?? new Exception(exception?.ToString());

            logEvent.Exception = exc;
            logEvent.Properties["tag"] = tag;
            
            Log.Log(logEvent);
        }

        public static void Error(string message, string tag = null, [CallerFilePath] string fileName = "", Exception exception = null)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                tag = GenerateTag(fileName);
            }

            var logEvent = new LogEventInfo(LogLevel.Error, "", message);
            logEvent.Properties["tag"] = tag;

            Log.Log(logEvent);

            SendLogEntryMessage(tag, message, ELogEntryType.Error, exception);
        }

        public static void Warn(string message, string tag = null, [CallerFilePath] string fileName = "")
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                tag = GenerateTag(fileName);
            }

            var logEvent = new LogEventInfo(LogLevel.Warn, "", message);
            logEvent.Properties["tag"] = tag;

            Log.Log(logEvent);

            SendLogEntryMessage(tag, message, ELogEntryType.Warning);
        }

        public static void Debug(string message, string tag = null, [CallerFilePath] string fileName = "")
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                tag = GenerateTag(fileName);
            }

            var logEvent = new LogEventInfo(LogLevel.Debug, "", message);
            logEvent.Properties["tag"] = tag;

            Log.Log(logEvent);

            SendLogEntryMessage(tag, message, ELogEntryType.Debug);
        }

        public static void Info(string message, string tag = null, [CallerFilePath] string fileName = "")
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                tag = GenerateTag(fileName);
            }

            var logEvent = new LogEventInfo(LogLevel.Info, "", message);
            logEvent.Properties["tag"] = tag;

            Log.Log(logEvent);

            SendLogEntryMessage(tag, message, ELogEntryType.Info);
        }

        public static void Trace(string message, string tag = null, [CallerFilePath] string fileName = "")
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                tag = GenerateTag(fileName);
            }

            var logEvent = new LogEventInfo(LogLevel.Trace, "", message);
            logEvent.Properties["tag"] = tag;

            Log.Log(logEvent);
        }

        private static void SendLogEntryMessage(string tag, string message, ELogEntryType entryType, Exception ex = null)
        {
            MessagingCenter.Send(
                new LogEntry
                {
                    Time = DateTime.Now,
                    Exception = ex,
                    Message = message,
                    Tag = tag,
                    EntryType = entryType
                },
                "LogEntry");
        }

        private static string GenerateTag(string fileName)
        {
            var match = Regex.Match(fileName, FileNamePattern);
            var tag = match.Success ? match.Groups[1].Value : null;

            return tag;
        }
    }
}
